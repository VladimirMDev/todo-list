import './App.css'
import { Container } from './components/Container/Container'
import { TodoList } from './components/TodoList/TodoList'

function App(): React.ReactElement {
  return (
    <>
      <Container>
        <TodoList />
      </Container>
    </>
  )
}

export default App
