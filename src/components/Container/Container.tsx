import cl from './Container.module.scss'

interface ContainerProps {
  children: React.ReactNode
}

export const Container: React.FC<ContainerProps> = ({ children }) => {
  return <div className={cl.container}>{children}</div>
}
