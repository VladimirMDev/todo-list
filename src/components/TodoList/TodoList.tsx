import { TodoFilter } from './components/TodoFilter/TodoFilter'
import { AddTodoModal } from './components/AddTodoModal/AddTodoModal'
import { List } from './components/List/List'
import { useFilterTodos } from './hooks/useFilterTodos'
import { useAppSelector } from '../../redux/reduxHooks'
import { selectTodos } from '../../redux/slices/todoSlice'
import { Counter } from './components/Counter/Counter'
import { getCompletedUncompletedCount } from './components/Counter/helpers/getCompletedUncompletedCount'
import cl from './TodoList.module.scss'

const TodoList = (): React.ReactElement => {
  const todos = useAppSelector(selectTodos)
  const { completed, uncompleted } = getCompletedUncompletedCount(todos)
  const { filteredTodos, changeFilterHandler } = useFilterTodos(todos)
  return (
    <section className={cl.todolist}>
      <h1 className={cl.title}>Todo List</h1>
      <div className={cl.controls}>
        <AddTodoModal />
        <TodoFilter handleChange={changeFilterHandler} />
      </div>
      <Counter completedCount={completed} uncompletedCount={uncompleted} />
      <List todos={filteredTodos} />
    </section>
  )
}

export { TodoList }
