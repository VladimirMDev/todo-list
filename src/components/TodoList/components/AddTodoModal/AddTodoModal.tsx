import { useState } from 'react'
import { useAppDispatch } from '../../../../redux/reduxHooks'
import { addTodo } from '../../../../redux/slices/todoSlice'
import {
  isTextLengthInLimit,
  TextOverLimitMessage,
} from '../../../../helpers/isTextLengthInLimit'
import { Button, Input, Modal, Tooltip } from 'antd'
import { clsx } from 'clsx'
import cl from './AddTodoModal.module.scss'

export const AddTodoModal: React.FC = () => {
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false)
  const [todoText, setTodoText] = useState<string>('')
  const textLengthStatus = isTextLengthInLimit(todoText)

  const dispatch = useAppDispatch()

  const handleShowModal = (): void => {
    setIsModalOpen(true)
  }

  const handleCloseModal = (): void => {
    setIsModalOpen(false)
  }

  const addTodoHandler = (): void => {
    if (textLengthStatus === 'valid') {
      const newTodo = { id: Date.now(), text: todoText, completed: false }
      dispatch(addTodo(newTodo))
      setTodoText('')
    }
  }

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setTodoText(e.target.value)
  }

  return (
    <>
      <Button
        className={cl['button-outer-modal']}
        type="primary"
        size={'large'}
        onClick={handleShowModal}
      >
        Add todo
      </Button>
      <Modal
        title="Add your todo"
        open={isModalOpen}
        onCancel={handleCloseModal}
        footer={
          <Button
            onClick={addTodoHandler}
            type="primary"
            size={'large'}
            disabled={textLengthStatus !== 'valid'}
          >
            Add todo
          </Button>
        }
      >
        <Tooltip
          title={textLengthStatus === 'overLimit' && TextOverLimitMessage}
        >
          <Input
            className={clsx(textLengthStatus === 'overLimit' && cl['wrong'])}
            value={todoText}
            onChange={handleChange}
            placeholder="Todo text"
          />
        </Tooltip>
      </Modal>
    </>
  )
}
