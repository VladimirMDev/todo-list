import cl from './Counter.module.scss'

interface ICounter {
  completedCount: number
  uncompletedCount: number
}

export const Counter: React.FC<ICounter> = ({
  completedCount,
  uncompletedCount,
}) => {
  return (
    <div className={`${cl['counter-wrapper']}`}>
      <span>Completed: {completedCount}</span>
      <span>Uncompleted: {uncompletedCount}</span>
    </div>
  )
}
