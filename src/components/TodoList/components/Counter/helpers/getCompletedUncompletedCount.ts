import { ITodo } from '../../../../../types/types'

export const getCompletedUncompletedCount = (
  todos: ITodo[],
): { completed: number, uncompleted: number } => {
  return todos.reduce(
    (acc, todo) => {
      if (todo.completed) {
        acc.completed++
      } else {
        acc.uncompleted++
      }
      return acc
    },
    { completed: 0, uncompleted: 0 },
  )
}
