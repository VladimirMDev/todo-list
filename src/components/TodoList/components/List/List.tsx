import { Todo } from '../TodoItem/Todo'
import { ITodo } from '../../../../types/types'

interface IList {
  todos: ITodo[]
}

export const List = ({ todos }: IList): React.ReactElement => {
  return (
    <div>
      {todos.map((todo) => {
        return (
          <Todo
            key={todo.id}
            id={todo.id}
            text={todo.text}
            completed={todo.completed}
          />
        )
      })}
    </div>
  )
}
