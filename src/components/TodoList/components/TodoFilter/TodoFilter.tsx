import cl from './TodoFilter.module.scss'
import { Select } from 'antd'
import { TFilterOptions } from '../../hooks/useFilterTodos'

interface ITodoFilter {
  handleChange: (value: TFilterOptions) => void
}

export const TodoFilter = ({
  handleChange,
}: ITodoFilter): React.ReactElement => {
  const options = [
    {
      value: 'all',
      label: <span>All </span>,
    },
    {
      value: 'completed',
      label: <span>Completed </span>,
    },
    {
      value: 'uncompleted',
      label: <span>Uncompleted </span>,
    },
  ]

  return (
    <div>
      <span className={cl['title']}>Show: </span>
      <Select
        className={cl['select']}
        defaultValue={'all'}
        onChange={handleChange}
        size={'large'}
        options={options}
      />
    </div>
  )
}
