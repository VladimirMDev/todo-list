import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  DeleteOutlined,
} from '@ant-design/icons'
import { useAppDispatch } from '../../../../redux/reduxHooks'
import {
  deleteTodo,
  toggleTodoStatus,
} from '../../../../redux/slices/todoSlice'
import { ITodo } from '../../../../types/types'
import { clsx } from 'clsx'
import cl from './Todo.module.scss'

export const Todo = ({ id, text, completed }: ITodo): React.ReactElement => {
  const dispatch = useAppDispatch()
  const statusChangeHandler = (): void => {
    dispatch(toggleTodoStatus(id))
  }

  const deleteHandler = (): void => {
    dispatch(deleteTodo(id))
  }

  return (
    <article
      onClick={statusChangeHandler}
      className={clsx(
        cl['todo-item'],
        completed ? cl['completed-todo'] : cl['uncompleted-todo'],
      )}
    >
      <span className={cl['icon']}>
        {completed ? <CheckCircleOutlined /> : <CloseCircleOutlined />}
      </span>
      <p className={cl['text']}>{text}</p>
      <button className={cl['delete-button']} onClick={deleteHandler}>
        <DeleteOutlined style={{ fontSize: '22px' }} />
      </button>
    </article>
  )
}
