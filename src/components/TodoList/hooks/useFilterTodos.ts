import { ITodo } from '../../../types/types'
import { useState } from 'react'

interface IFilteredTodosReturnTypes {
  changeFilterHandler: (newFilter: TFilterOptions) => void
  filteredTodos: ITodo[]
}

export const useFilterTodos = (todos: ITodo[]): IFilteredTodosReturnTypes => {
  const [currentFilter, setCurrentFilter] = useState<TFilterOptions>('all')
  const filteredTodos = todos.filter((todoItem): boolean => {
    switch (currentFilter) {
      case 'completed':
        return todoItem.completed
      case 'uncompleted':
        return !todoItem.completed
      default:
        return true
    }
  })

  const changeFilterHandler = (newFilter: TFilterOptions): void => {
    setCurrentFilter(newFilter)
  }

  return { changeFilterHandler, filteredTodos }
}

export type TFilterOptions = 'all' | 'completed' | 'uncompleted'
