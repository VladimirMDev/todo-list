const TEXTSIZE = 85

export const TextOverLimitMessage = `The todo text should be no more than ${TEXTSIZE} characters`

export const isTextLengthInLimit = (
  text: string,
): 'empty' | 'overLimit' | 'valid' => {
  if (text.length === 0) return 'empty'
  if (text.length > TEXTSIZE) return 'overLimit'
  return 'valid'
}
