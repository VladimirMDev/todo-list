import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { ITodo } from '../../types/types'
import { RootState } from '../store'

const initialState = {
  todos: [
    { id: 0, text: 'Todo 1', completed: false },
    { id: 1, text: 'Todo 2', completed: true },
    { id: 2, text: 'Todo 3', completed: false },
    { id: 3, text: 'Todo 4', completed: true },
  ],
}

export const todoSlice = createSlice({
  name: 'todoReducer',
  initialState,
  reducers: {
    addTodo: (state, action: PayloadAction<ITodo>) => {
      state.todos.push(action.payload)
    },
    deleteTodo: (state, action: PayloadAction<ITodo['id']>) => {
      state.todos = state.todos.filter((todo) => todo.id !== action.payload)
    },
    toggleTodoStatus: (state, action: PayloadAction<ITodo['id']>) => {
      state.todos = state.todos.map((todo) => {
        if (todo.id === action.payload) {
          todo.completed = !todo.completed
        }
        return todo
      })
    },
  },
})

export const { addTodo, deleteTodo, toggleTodoStatus } = todoSlice.actions

export const selectTodos = createSelector(
  (state: RootState) => state.todos.todos,
  (todos): ITodo[] => todos,
)

export default todoSlice.reducer
